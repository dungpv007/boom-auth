import * as crypto from "crypto";

export class Utils {

    // public static String getHPass(String pass) {
    //     long salt = System.currentTimeMillis() % 1000;
    //     return String.format("1,,%s,%s", salt, Util.getMD5(Util.getMD5(pass) + salt));
    //     }

    static assignObject = (target: any, source: any) =>{
        Object.keys(source).forEach(function(key) {
            if (key in target.definition.properties) { // or obj1.hasOwnProperty(key)
                target[key] = source[key];
            }
        });
    }

    static getHPass = (password : string) : string => {
        const md5  = crypto.createHash("md5").update(password).digest("hex").toString();
        return md5;
    }

    static isValidPassword = (password : string) : boolean => {
        for (var i = 0 ; i < password.length ; i++) {
            var ch = password.charCodeAt(i);
            if (ch < 33 || ch > 126) {
                return false;
            }
        }
        return true;
    }
}