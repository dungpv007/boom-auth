import { Count, CountSchema, Filter, FilterExcludingWhere, repository, Where, }                     from '@loopback/repository';
import { del, get, getModelSchemaRef, HttpErrors, param, patch, post, put, requestBody, response, } from '@loopback/rest';
import { LoginRequest, LoginResponse, Register, User }                                              from '../models';
import { SessionRepository, UserRepository }                                                        from '../repositories';
import { Utils }                                                                                    from "../utils/Utils";
import { service }                                                                                  from "@loopback/core";
import { v4 as uuidv4 }                                                                             from 'uuid';

export class AuthControllerController {
    constructor (
        @repository(UserRepository)
        public userRepository : UserRepository,
        @service(SessionRepository)
        public sessionRepository : SessionRepository,
    ) {}

    @post('/auth')
    @response(200, {
        description: 'User model instance',
        content    : { 'application/json': { schema: getModelSchemaRef(User) } },
    })
    async create (
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(User, {
                        title  : 'NewUser',
                        exclude: [ 'id' ],
                    }),
                },
            },
        })
            user : Omit<User, 'id'>,
    ) : Promise<User> {
        return this.userRepository.create(user);
    }

    @post('/register')
    @response(200, {
        description: 'register new user',
        content    : { 'application/json': { schema: getModelSchemaRef(User) } },
    })
    async register (
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(Register, {
                        title: 'Register'
                    }),
                },
            },
        })
            req : Register,
    ) : Promise<User> {
        if (!Utils.isValidPassword(req.password)) {
            throw new HttpErrors.BadRequest;
        }
        const user = new User();
        Object.assign(user, req);
        //@ts-ignore
        delete user["password"];
        // Object.keys(req).forEach(function(key) {
        //     if (key in User.definition.properties) { // or obj1.hasOwnProperty(key)
        //         //@ts-ignore
        //         user[key] = req[key];
        //     }
        // });
        user.hpass = Utils.getHPass(req.password);
        return this.userRepository.create(user);
    }

    @get('/login')
    @response(200, {
        description: 'login',
        content    : { 'application/json': { schema: getModelSchemaRef(User) } },
    })
    async login (
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(LoginRequest, {
                        title: 'LoginRequest'
                    }),
                },
            },
        })
            req : LoginRequest,
    ) : Promise<LoginResponse> {
        if (!Utils.isValidPassword(req.password)) {
            throw new HttpErrors.BadRequest;
        }
        const user = ( await this.userRepository.find({ where: { username: req.username } }) ).pop();
        console.log("user is: " + user);
        if (!user) {
            throw new HttpErrors.Unauthorized;
        }
        console.log("do check password");
        if (!( user.hpass === Utils.getHPass(req.password) )) {
            throw new HttpErrors.Unauthorized;
        }
        let session = uuidv4();
        this.sessionRepository.save(user.username, session);

        return new LoginResponse({ user: user, sessionId: session });
    }

    @get('/auth/count')
    @response(200, {
        description: 'User model count',
        content    : { 'application/json': { schema: CountSchema } },
    })
    async count (
        @param.where(User) where? : Where<User>,
    ) : Promise<Count> {
        return this.userRepository.count(where);
    }

    @get('/auth')
    @response(200, {
        description: 'Array of User model instances',
        content    : {
            'application/json': {
                schema: {
                    type : 'array',
                    items: getModelSchemaRef(User, { includeRelations: true }),
                },
            },
        },
    })
    async find (
        @param.filter(User) filter? : Filter<User>,
    ) : Promise<User[]> {
        return this.userRepository.find(filter);
    }

    @patch('/auth')
    @response(200, {
        description: 'User PATCH success count',
        content    : { 'application/json': { schema: CountSchema } },
    })
    async updateAll (
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(User, { partial: true }),
                },
            },
        })
            user : User,
        @param.where(User) where? : Where<User>,
    ) : Promise<Count> {
        return this.userRepository.updateAll(user, where);
    }

    @get('/auth/{id}')
    @response(200, {
        description: 'User model instance',
        content    : {
            'application/json': {
                schema: getModelSchemaRef(User, { includeRelations: true }),
            },
        },
    })
    async findById (
        @param.path.number('id') id : number,
        @param.filter(User, { exclude: 'where' }) filter? : FilterExcludingWhere<User>
    ) : Promise<User> {
        return this.userRepository.findById(id, filter);
    }

    @patch('/auth/{id}')
    @response(204, {
        description: 'User PATCH success',
    })
    async updateById (
        @param.path.number('id') id : number,
        @requestBody({
            content: {
                'application/json': {
                    schema: getModelSchemaRef(User, { partial: true }),
                },
            },
        })
            user : User,
    ) : Promise<void> {
        await this.userRepository.updateById(id, user);
    }

    @put('/auth/{id}')
    @response(204, {
        description: 'User PUT success',
    })
    async replaceById (
        @param.path.number('id') id : number,
        @requestBody() user : User,
    ) : Promise<void> {
        await this.userRepository.replaceById(id, user);
    }

    @del('/auth/{id}')
    @response(204, {
        description: 'User DELETE success',
    })
    async deleteById (@param.path.number('id') id : number) : Promise<void> {
        await this.userRepository.deleteById(id);
    }
}
