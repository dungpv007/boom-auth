import { Model, model, property } from '@loopback/repository';
import { User }                   from "./user.model";

@model()
export class LoginResponse extends Model {
    @property({
        type: 'object',
    })
    user? : User;

    @property({
        type: 'string',
    })
    sessionId? : string;


    constructor (data? : Partial<LoginResponse>) {
        super(data);
    }
}
