import {Model, model, property} from '@loopback/repository';

@model()
export class Session extends Model {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
  })
  id: string;

  @property({
    type: 'string',
  })
  username?: string;


  constructor(data?: Partial<Session>) {
    super(data);
  }
}
