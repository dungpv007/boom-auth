import { Model, model, property } from '@loopback/repository';

@model()
export class Register extends Model {
    @property({
        type    : 'string',
        required: true,
    })
    username : string;

    @property({
        type    : 'string',
        required: true,
    })
    password : string;

    @property({
        type    : 'string',
        required: false,
    })
    mobile? : string;

    @property({
        type    : 'string',
        required: false,
    })
    email? : string;

    @property({
        type    : 'string',
        required: false,
    })
    os? : string;

    constructor (data? : Partial<Register>) {
        super(data);
    }
}
