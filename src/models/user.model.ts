import {Entity, model, property} from '@loopback/repository';

@model({name: "auth_user"})
export class User extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id: number;

  @property({
    type: 'number',
    mysql: {
      columnName: 'created_on',
      dataType: 'timestamp',
      nullable: 'Y',
    }
  })
  createdOn?: number;

  @property({
    type: 'string',
    required: true,
  })
  username: string;

  @property({
    type: 'string',
  })
  facebook?: string;

  @property({
    type: 'string',
    hidden: true
  })
  hpass?: string;

  @property({
    type: 'string',
  })
  cp?: string;

  @property({
    type: 'string',
  })
  subcp?: string;

  @property({
    type: 'string',
  })
  mobile?: string;

  @property({
    type: 'string',
  })
  os?: string;

  @property({
    type: 'string',
    mysql: {
      columnName: 'os_version',
      dataType: 'varchar',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'Y',
    }
  })
  osVersion?: string;

  @property({
    type: 'number',
    mysql: {
      columnName: 'last_login',
      dataType: 'timestamp',
      nullable: 'Y',
    }
  })
  lastLogin?: number;

  @property({
    type: 'string',
  })
  udid?: string;

  @property({
    type: 'string',
  })
  version?: string;

  @property({
    type: 'string',
  })
  email?: string;

  @property({
    type: 'string',
    mysql: {
      columnName: 'server_ids',
      dataType: 'varchar',
      dataLength: 255,
      dataPrecision: null,
      dataScale: null,
      nullable: 'Y',
    }
  })
  serverIds?: string;


  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
