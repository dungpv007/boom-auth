import Memcached                    from "memcached";
import { memcachedConfig }          from "../config.json";
import { BindingScope, injectable } from "@loopback/core";

@injectable({ scope: BindingScope.TRANSIENT })
export class SessionRepository {
    public cache : Memcached;

    constructor () {
        this.cache = new Memcached(memcachedConfig.hosts, memcachedConfig.options);
    }

    public save (username : string, session : string) {
        console.log("save session to cache for: " + username);
        this.cache.set("s:" + username, session, 600, (err, rs) => {
            console.log("memcached result: " + username + " = " + rs);
        })
    }
}
